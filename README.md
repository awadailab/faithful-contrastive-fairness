# Faithful Contrastive Fairness

Official implementation of the paper: Beyond Model Interpretability: On the Faithfulness and Adversarial Robustness of Contrastive Textual Explanations


## Abstract
This work motivates textual counterfactuals by laying the ground for a novel evaluation scheme inspired by the faithfulness of explanations. Accordingly, we extend the computation of three metrics, proximity, connectedness and stability, to textual data and we benchmark two successful contrastive methods, POLYJUICE and MiCE, on our suggested metrics. Experiments on sentiment analysis data show that the connectedness of counterfactuals to their original counterparts is not obvious in both models. More interestingly, the generated contrastive texts are more attainable with POLYJUICE which highlights the significance of latent representations in counterfactual search. Finally, we perform the first semantic adversarial attack on textual recourse methods. The results demonstrate the robustness of POLYJUICE and the role that latent input representations play in robustness and reliability. 

## Installation
```
git clone ...
cd faithful_nlp 
pip install requirements.txt
```

## Code structure 

-  notebooks: example usage to compute 
    - faithfulness as:
        - proximity
        - connectedness
        - stability
    - existing metrics: 
        - BLEU
        - Self-BERT
- explanation: 
    - explanations derived by MiCE and POLYJUICE
    - adversarial attacks on explanations
- data: original dataset



## License
Open source project for non commercial use

## Paper: 
Preprit on https://arxiv.org/abs/2210.08902

## Citation
Zini, Julia El, and Mariette Awad. "Beyond Model Interpretability: On the Faithfulness and Adversarial Robustness of Contrastive Textual Explanations." Findings of Emprical Methods in Natural Language Processing (EMNLP), (2022).

```
@article{zini2022beyond,
  title={Beyond Model Interpretability: On the Faithfulness and Adversarial Robustness of Contrastive Textual Explanations},
  author={Zini, Julia El and Awad, Mariette},
  journal={Findings of Emprical Methods in Natural Language Processing (EMNLP)},
  year={2022}
}
```
